package com.devcamp.s50.invoice_restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvoiceRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvoiceRestapiApplication.class, args);
	}

}

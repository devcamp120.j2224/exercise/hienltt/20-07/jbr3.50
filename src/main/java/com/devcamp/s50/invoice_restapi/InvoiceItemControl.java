package com.devcamp.s50.invoice_restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InvoiceItemControl {
    @CrossOrigin
    @GetMapping("/invoices")
    public  ArrayList<InvoiceItem> getListInvoiceItem(){
        InvoiceItem invoice1 = new InvoiceItem(1, "tivi", 2, 20000000);
        InvoiceItem invoice2 = new InvoiceItem(2, "máy giặt", 3, 11000000);
        InvoiceItem invoice3 = new InvoiceItem(3, "tủ lạnh", 5, 8000000);
        System.out.println(invoice1);
        System.out.println(invoice2);
        System.out.println(invoice3);

        ArrayList<InvoiceItem> listInvoice = new ArrayList<>();
        listInvoice.add(invoice1);
        listInvoice.add(invoice2);
        listInvoice.add(invoice3);
        
        return listInvoice;
    }
}
